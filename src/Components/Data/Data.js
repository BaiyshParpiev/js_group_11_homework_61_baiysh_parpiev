import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {COUNTRY_URL} from "../../config";
import './Data.css';

const Data = props => {
    const [data, setData] = useState({});
    console.log(props.code)
    useEffect(() => {
        const fetchData = async() => {
            if(props.code === null) return;
            try{
                const response = await axios.get(COUNTRY_URL + props.code);
                const data = response.data;
                setData(data);
            }catch(e) {
                console.log(e.response);
            }
        };

        fetchData().catch(e => console.error(e));
    }, [props.code])

    return data && (
        <div className="block">
          <h2>Name of Country: {data.name}</h2>
            <img src={`https://restcountries.eu/data/${props.code ? props.code.toLowerCase() : 'arg'}.svg`}/>
            <h3> Name of Capital City{data.capital}</h3>
          <p>Region: {data.region}</p>
            <p>Native name: {data.nativeName}</p>
        </div>
    );
};

export default Data;