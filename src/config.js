const BASE_URL = 'https://restcountries.eu/'
export const COUNTRY_URL = BASE_URL+ '/rest/v2/alpha/';
export const COUNTRIES_URL = BASE_URL + '/rest/v2/all?fields=name%3Balpha3Code'
