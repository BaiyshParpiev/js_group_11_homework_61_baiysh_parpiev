import './App.css';
import Builder from './Builder/Builder';

function App() {
  return (
    <Builder/>
  );
}

export default App;
