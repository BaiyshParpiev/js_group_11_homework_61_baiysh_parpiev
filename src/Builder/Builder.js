import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {COUNTRIES_URL} from '../config';
import Data from "../Components/Data/Data";
 
const Builder = () => {
    const [countries, setCountries] = useState([]);
    const [code, setCode] = useState();

    useEffect(() => {
        const fetchData = async() => {
            try{
                const response = await axios.get(COUNTRIES_URL);
                const countries = response.data;
                setCountries(countries);
                console.log(countries);
            }catch(e) {
                console.log(e.response);
            }
        };

        fetchData().catch(e => console.error(e));
    }, [])

    return (
        <div className="container">
           <ul className="list">
               {countries.map(d=> (
                   <li key={countries.indexOf(d)} onClick={() => setCode(d.alpha3Code)}>{d.name}</li>
               ))}
           </ul>
            <Data code={code}/>
        </div>
    );
};

export default Builder;